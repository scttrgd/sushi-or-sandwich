import random
import time

from PIL import Image
from flask import Flask
from flask import jsonify
from flask import request
from flask_cors import CORS

app = Flask('FakeSushiOrSandwich')
# Added cors support to allow cross origin fetching (as the API might be hosted on a different IP to the web client)
cors = CORS(app)


@app.route('/predict', methods=['POST'])
def predict():
    """
        Given a user uploaded image, returns whether it is a sushi or a sandwich (string 'sushi' or 'sandwich')

        Endpoint:
            example: curl -X POST -F "image=@/Users/myusername/Downloads/burger_3.jpg" http://0.0.0.0:5000/predict

        Returns:
            json of format {'output': 'sushi'}
    """

    img_file = request.files['image']
    _ = Image.open(img_file)

    # Fake prediction
    time.sleep(1)
    prediction = 'sushi' if random.random() < 0.5 else 'sandwich'

    return jsonify(output=prediction), 200, {'ContentType': 'application/json'}


if __name__ == '__main__':
    app.run(host='0.0.0.0')
