FROM python:3.7
WORKDIR /workspace

RUN pip install -U pip

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

EXPOSE 5000

ENTRYPOINT ["python3", "sushi_or_sandwich_server.py"]