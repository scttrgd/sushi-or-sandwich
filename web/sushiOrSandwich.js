// Some constants to grab the value of elements on the page
const userUpload = document.getElementById('user-upload');
const imagePlaceholder = document.getElementById('imagePlaceholder');
const sushiResult = document.getElementById('sushiResult');
const header = document.getElementById('header');
// Set the value of fetchAddress to your docker host's IP address for access outside your local machine
const fetchAddress = 'http://0.0.0.0:5000/predict';
document.querySelector("#sushiForm").addEventListener("submit", function(event) {
  event.preventDefault();
  sushiSend();
}, false);
// This function is called on the onClick event of the button on the web page
function sushiSend(e) {
  // Hide the header before showing it again
  // This only really has an impact if the user is performing an additional check
  header.setAttribute('hidden', '');
  // Disable the button and change the text to indicate that the upload process is taking place
  document.getElementById('upload').setAttribute('disabled', '');
  document.getElementById('upload').innerHTML = 'Uploading...';
  // FormData is used to assign key/value pairs. In our case this will be 'image' and the image file at position 0 in the files array (there's only one image)
  let formData = new FormData();
  let fileField = document.querySelector("input[type='file']");
  imagePlaceholder.src = URL.createObjectURL(fileField.files[0]);
  formData.append('image', fileField.files[0]);
  // Send the request using the HTML fetch API with the value of formData as the body
  fetch(fetchAddress, {
    method: 'POST',
    body: formData
  })
  // Take the fetch response and execute a function
  .then(function(response){
      response.json().then(function(data) {
      var sushiOrSammy = data.output;
      // This if statement checks the value of sushiOrSammy and changes the plurality
      if (sushiOrSammy == 'sandwich') {
        var sushiOrSammy = 'a sandwich';
      }
      else if (sushiOrSammy == 'sushi'){
        var sushiOrSammy = 'sushi';
      }
      else {
        var sushiOrSammy = "something I don't recognise "
      }
      // Add text to the header that the image is of either sushi or sandwich
      // unhide the header div and add an alt tag to the image for accessibility
      sushiResult.innerHTML = "👇 That's "+ sushiOrSammy+ " 👇";
      imagePlaceholder.setAttribute('alt', 'An image of '+sushiOrSammy);
      header.removeAttribute('hidden');
      // Re-enable the button once the response bits have been shown
      document.getElementById('upload').removeAttribute('disabled');
      document.getElementById('upload').innerHTML = 'Upload';
      });
  });
}
