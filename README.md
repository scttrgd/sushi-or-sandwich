## Cookpad Technologist Technical Assignment

Imagine that you are working at Cookpad on a strategic long-term project which aims to teach people how to cook better sushi
and sandwiches. One day, an excited Machine Learning researcher runs to your desk, claiming that they have got the
ultra-deep-learning model that can, given an image, determine whether this is a sushi or a sandwich. The researcher believes
that releasing such a model would make a massive impact on an average cook, whose life will get transformed by the ability
to analyze images of sushis and sandwiches made at their homes.

Unfortunately, his ability to design and implement any user-facing web prototype is nearly zero. The best he could come
up with, was this sketch, along with the rough explanation that the prototype "Should allow the user to upload the
image and see on the screen whether it is a sushi, or a sandwich".

![sketch](sketch.jpg)


Fortunately, the researcher at least managed to create a basic Docker container, which runs a Python Flask server
('model server') which in turn exposes the machine learning model to the client via the endpoint /predict.

Could you help the researcher and
implement a simple web server and UX that allows the user to upload an image from the browser,  calls the 'model server'
and displays whether the result is 'sushi' or 'sandwich'? This system will be shown to a few test users only, so there are no
performance requirements and you can use any web framework or language that you prefer. We don't look for anything fancy -
any minimal design would do, and we don't expect you to spend more than a couple of hours on it.

Your submission should contain documentation that will allow us to understand your work and run your code i.e. a README
and any other supplementary material not provided by us.

Within the README.md
- Describe your approach and mention which technologies/frameworks you decided to use and why.
- Describe how to run your code.

Key points:
- Please make sure your code is legible and instructions how to run it are clear.
- Try not spend more than a couple of hours on the solution - we are really looking for the most basic minimal product
- The description on how to run the 'model server' is below

'Model Server' Description
------------

'Model Server' is an http server that generates the prediction of whether the supplied image is a sushi or a sandwich.

Note: for the purposes of this assignment, the server just processes the image and returns a random answer without any machine learning.

#### How to run the server

```
docker build -t sushi_or_sandwich_server .
docker run -p 5000:5000 -it sushi_or_sandwich_server
```

Alternatively, you can just run ```python sushi_or_sandwich_server.py``` if your python3 environment is set up and
Flask is installed.

#### How to query the server (example)

```
curl -X POST -F "image=@/Users/myusername/Downloads/burger_3.jpg" http://0.0.0.0:5000/predict
```

'Web Client' Description
------------

- I decided to build the web client using HTML and vanilla javascript in order to keep things as straightforward as possible.
- I've chosen to use Twitter's Bootstrap front-end CSS framework as it gives a quick and straightforward way of create a decent user experience.
- I've used the 'fetch' HTML5 API to send data to the python mock API as this requires the installation of no additional JS libraries and has pretty decent support (apart from IE and IE Mobile)
- I've chosen to create an additional dockerfile in order to run the web client. The dockerfile takes the contents of the 'web' directory and copies it into the default alpine nginx directory. It then exposes port 80 to the host system (when running the container )
- The user's image never strictly gets uploaded anywhere; The JS gets the value of the image file and sends that 
    - This may have some memory impact if the user uploads hundreds of images without reloading the page but shouldn't be a problem for testing the solution

#### The process itself

```mermaid
graph LR
A(user uploads image) --> B(JS picks up image)
B --> C(User clicks upload button)
C --> D(JS sends API request using fetch)
D --> E(JS displays value of the output in a header tag)
E --> A(User can upload additional images)
```

![Web Process Description](web-api-process.png)

#### How to run the web server

ensure you're the 'web' directory
```
docker build -t sushi_or_sandwich_web .
docker run -p 8080:80 -it sushi_or_sandwich_web
```

#### Accessing the website

If you intend to access the website on the machine that is hosting both docker containers:
- navigate to localhost:8080 in any browser

If you intend to access the website on any machine that is not hosting the docker containers:
- in sushiOrSandwich.js set the value of fetchAddress to the IP address of the API container's host machine
- navigate to port 8080 on the web container's IP address (192.168.1.127:8080)

#### Live Example

An example of how the site should behave can be found [here](https://sushiorsandwich.herokuapp.com) with the API being hosted [here](https://sushiorsandwichapi.herokuapp.com/predict) (this is not hosted at port 5000 due to the default heroku entry port being 443)

I opted to use heroku to host the site and API as:
1. It's free for small projects
2. Once a site is up and running on there the uptime is pretty good
3. It didn't require writing a huge amount of extra code or much extra setting up (Great for proof of concepts!)

- There's an extra [Procfile](Procfile) file in order for heroku to build the API
- There's also a [static.json](static.json) file for the rendering of the static HTML site (using the heroku static site buildpack)

Additional Notes
-----------

- I've added an extra library (flask_cors) to the sushi_or_sandwich_server.py file in order to allow the web client to access the API server on Chrome (there's a bug in Chrome that causes localhost requests to fail)
  - The CORS library also allows the web server container to run on a different host to the API server container